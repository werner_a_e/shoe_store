-- Your SQL goes here
CREATE TABLE sales (
   id INTEGER PRIMARY KEY NOT NULL, 
   date VARCHAR NOT NULL,
   tax_total INTEGER NOT NULL,
   sub_total INTEGER NOT NULL,
   total INTEGER NOT NULL
);

CREATE TABLE product_sales (
   id INTEGER PRIMARY KEY NOT NULL, 
   sale_id INTEGER NOT NULL,
   product_id INTEGER NOT NULL,
   product_count DOUBLE NOT NULL,
   price INTEGER NOT NULL,
   tax INTEGER NOT NULL,
   discount INTEGER NOT NULL,
   sub_total INTEGER NOT NULL,
   FOREIGN KEY(sale_id) REFERENCES sales(id) ON DELETE CASCADE,
   FOREIGN KEY(product_id) REFERENCES products(id) ON DELETE CASCADE
);