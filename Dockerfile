FROM rust:1.51.0

WORKDIR /usr/src
RUN apt-get install -y libsqlite3-dev
RUN cargo install diesel_cli --no-default-features --features sqlite
RUN mkdir /usr/src/project
COPY . /usr/src/project
WORKDIR /usr/src/project
RUN cargo build
RUN cargo run
