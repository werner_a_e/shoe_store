#[macro_use]
extern crate diesel;
use shoe_store::schema::sales;
use serde::{Serialize, Deserialize};

#[derive(Insertable, Debug, AsChangeset, Serialize, Deserialize)]
#[table_name="sales"]
pub struct NewSale {
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

#[derive(Identifiable, Queryable, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[table_name = "sales"]
pub struct Sale {
    pub id: i32,
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

fn create_sale(sale_str: String, conn: &SqliteConnection) -> Result<usize, Error> {
    let new_sale: NewSale = serde_json::from_str(&sale_str).unwrap();
    
    use ::shoe_store::schema::sales::dsl::*;
    diesel::insert_into(sales)
        .values(NewSale {
            date: new_sale.date,
            tax_total: new_sale.tax_total,
            sub_total: new_sale.sub_total,
            total: new_sale.tax_total + new_sale.sub_total
        })
        .execute(conn)
}

use diesel::{QueryDsl, RunQueryDsl};

fn show_sale(sale_id: i32, conn: &SqliteConnection) -> Result<Sale, Error> {
    sales::dsl::sales
        .find(sale_id)
        .first::<Sale>(conn)
}

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use diesel::sqlite::SqliteConnection;
use diesel::Connection;
use ::shoe_store::establish_connection_test;
use diesel::result::Error;
use anyhow::Result;

no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

fn main() {
    if let Ok(lines) = read_lines("inputs.txt") {
        let connection = establish_connection_test();
        connection.test_transaction::<_, Error, _>(|| {
            for line in lines {
                if let Ok(new_sale_str) = line {
                    create_sale(new_sale_str, &connection)?;
                    let last_sale_id: i32 = diesel::select(last_insert_rowid).first(&connection)?;                    
                    let sale = show_sale(last_sale_id, &connection)?;
                    println!("{:#?}", serde_json::to_string(&sale).unwrap());
                }
            }
            Ok(())
        });
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

// segunda pregunta
#[macro_use]
extern crate diesel;
use shoe_store::schema::sales;
use serde::{Serialize, Deserialize};

#[derive(Insertable, Debug, AsChangeset, Serialize, Deserialize)]
#[table_name="sales"]
pub struct FormSale {
    pub date: String,
    pub tax_total: Option<i32>,
    pub sub_total: Option<i32>,
    pub total: Option<i32>
}

fn update_sale(sale_id: i32, form_sale: FormSale, conn: &SqliteConnection) -> Result<usize, Error> {
    use ::shoe_store::schema::sales::dsl::sales;

    diesel::update(sales.find(sale_id))
        .set(&form_sale)
        .execute(conn)
}

fn delete_sale(sale_id: i32, conn:  &SqliteConnection) -> Result<usize, Error> {
    use ::shoe_store::schema::sales::dsl::sales;
    diesel::delete(sales.find(sale_id))
        .execute(conn)
}

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use diesel::sqlite::SqliteConnection;
use diesel::Connection;
use ::shoe_store::establish_connection_test;
use diesel::result::Error;
use anyhow::Result;

#[derive(Identifiable, Queryable, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[table_name = "sales"]
pub struct Sale {
    pub id: i32,
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

#[derive(Insertable, Debug, AsChangeset, Serialize, Deserialize)]
#[table_name="sales"]
pub struct NewSale {
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}
no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

fn main() {
    let mut count_for_deletion = 1;
    let mut count_for_date = 1;
    if let Ok(lines) = read_lines("inputs_for_update.txt") {
        let connection = establish_connection_test();
        connection.test_transaction::<_, Error, _>(|| {
            for line in lines {
                if let Ok(new_sale_str) = line {
                    create_sale(new_sale_str, &connection)?;
                    let last_sale_id: i32 = diesel::select(last_insert_rowid).first(&connection)?;
                    update_sale(last_sale_id, FormSale {
                        date: format!("0{}/01/2020", count_for_date).to_string(),
                        tax_total: None,
                        sub_total: None,
                        total: None
                    }, &connection).unwrap();
                    if count_for_deletion % 2 == 0 {
                        delete_sale(last_sale_id, &connection).unwrap();
                    } else {
                        let sale = show_sale(last_sale_id, &connection)?;
                        println!("{:#?}", serde_json::to_string(&sale).unwrap());
                        count_for_date += 1;
                    }
                }
                count_for_deletion += 1;
            }
            Ok(())
        });
    }
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}


fn create_sale(sale_str: String, conn: &SqliteConnection) -> Result<usize, Error> {
    let new_sale: NewSale = serde_json::from_str(&sale_str).unwrap();
    
    use ::shoe_store::schema::sales::dsl::*;
    diesel::insert_into(sales)
        .values(NewSale {
            date: new_sale.date,
            tax_total: new_sale.tax_total,
            sub_total: new_sale.sub_total,
            total: new_sale.tax_total + new_sale.sub_total
        })
        .execute(conn)
}

use diesel::{QueryDsl, RunQueryDsl};

fn show_sale(sale_id: i32, conn: &SqliteConnection) -> Result<Sale, Error> {
    sales::dsl::sales
        .find(sale_id)
        .first::<Sale>(conn)
}