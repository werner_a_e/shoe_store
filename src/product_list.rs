use actix_web::{web, App, HttpServer, Responder};
use diesel::sqlite::SqliteConnection;
use serde::Deserialize;
use ::shoe_store::models::*;
use anyhow::Result;
use diesel::result::Error;
use actix_cors::Cors;

#[derive(Deserialize)]
struct QueryParamsList {
	limit: i64
}

async fn product_list(query_param_list: web::Query<QueryParamsList>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match list_products(query_param_list.limit, &conn) {
		Ok(products) => Ok(web::Json(products)),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

use diesel::BelongingToDsl;
use diesel::GroupedBy;
use diesel::{RunQueryDsl, QueryDsl};

fn list_products(limit: i64, conn: &SqliteConnection) -> Result<Vec<(Product, Vec<(ProductVariant, Variant)>)>, Error> {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::variants;

    let products_result = 
        products
        .limit(limit)
        .load::<Product>(conn)?;
    let variants_result =
        ProductVariant::belonging_to(&products_result)
            .inner_join(variants)
            .load::<(ProductVariant, Variant)>(conn)?
            .grouped_by(&products_result);
    let data = products_result.into_iter().zip(variants_result).collect::<Vec<_>>();

    Ok(data)
}

use ::shoe_store::establish_connection;
#[actix_web::main]
async fn main() -> std::io::Result<()> {

    HttpServer::new(|| {
        let cors = Cors::permissive();
        App::new()
			.wrap(cors)
			.data(establish_connection())
            .route("products", web::get().to(product_list))
    })
    .bind(("127.0.0.1", 9400))?
    .run()
    .await
}
