use serde::{Serialize, Deserialize};

#[derive(Identifiable, Queryable, Clone, Debug, Serialize, Deserialize)]
#[table_name = "products"]
pub struct Product {
    pub id: i32,
    pub name: String,
    pub cost: f64,
    pub active: bool,
}

use super::schema::products;

#[derive(Insertable, Debug, AsChangeset, Serialize, Deserialize, Clone)]
#[table_name="products"]
pub struct NewProduct {
    pub name: String,
    pub cost: f64,
    pub active: bool,
}

#[derive(Identifiable, Queryable, Clone, Debug, Serialize, Deserialize)]
#[table_name = "variants"]
pub struct Variant {
    pub id: i32,
    pub name: String,
}

use super::schema::variants;

#[derive(Insertable, Debug, Clone, Serialize, Deserialize)]
#[table_name="variants"]
pub struct NewVariant {
    pub name: String,
}

use super::schema::products_variants;

#[derive(Insertable, Debug)]
#[table_name="products_variants"]
pub struct NewProductVariant {
    pub variant_id: i32,
    pub product_id: i32,
    pub value: Option<String>
}

#[derive(Insertable, Queryable, AsChangeset, Debug, Clone, Serialize, Deserialize)]
#[table_name="variants"]
pub struct FormVariant {
    pub id: Option<i32>,
    pub name: String
}

#[derive(Insertable, Debug, AsChangeset, Clone)]
#[table_name="products_variants"]
pub struct FormProductVariant {
    pub id: Option<i32>,
    pub variant_id: Option<i32>,
    pub product_id: i32,
    pub value: Option<String>
}

#[derive(Clone)]
pub struct FormProductVariantComplete {
    pub variant: Option<FormVariant>,
    pub product_variant: FormProductVariant,
}

#[derive(Clone)]
pub struct FormProduct {
    pub product: NewProduct,
    pub variants: Vec<FormProductVariantComplete>
}

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct NewVariantValue {
    pub variant: NewVariant,
    pub values: Vec<Option<String>>
}

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct NewCompleteProduct {
    pub product: NewProduct,
    pub variants: Vec<NewVariantValue>
}

#[derive(Identifiable, Associations,  Queryable, Debug, Clone, Serialize, Deserialize, PartialEq)]
#[belongs_to(Product)]
#[belongs_to(Variant)]
#[table_name="products_variants"]
pub struct ProductVariant {
    pub id: i32,
    pub variant_id: i32,
    pub product_id: i32,
    pub value: Option<String>
}