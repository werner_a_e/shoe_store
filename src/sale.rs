
use shoe_store::schema::sales;
use serde::{Serialize, Deserialize};

#[derive(Identifiable, Queryable, Clone, Debug, Serialize, Deserialize)]
#[table_name = "sales"]
pub struct Sale {
    pub id: i32,
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

#[derive(Insertable, Debug, AsChangeset)]
#[table_name="sales"]
pub struct NewSale {
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

use diesel::sqlite::SqliteConnection;
use diesel::Connection;
use ::shoe_store::establish_connection_test;
use diesel::result::Error;
use anyhow::Result;

fn create_sale(new_sale: NewSale, conn: &SqliteConnection) -> Result<usize, Error> {
    use ::shoe_store::schema::sales::dsl::*;
    diesel::insert_into(sales)
        .values(NewSale {
            date: new_sale.date,
            tax_total: new_sale.tax_total,
            sub_total: new_sale.sub_total,
            total: new_sale.tax_total + new_sale.sub_total
        })
        .execute(conn)
}

use ::shoe_store::establish_connection;
use crate::diesel::RunQueryDsl;
use crate::diesel::QueryDsl;

fn list_sales(conn: &SqliteConnection) -> Vec<Sale> {
    use ::shoe_store::schema::sales::dsl::*;
    sales 
        .limit(10)
        .load::<Sale>(conn)
        .expect("Error loading sales")
}

#[test]
fn create_sale_test() {
    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        create_sale(NewSale {
            date: "01/01/2020".to_string(),
            tax_total: 210_000,
            sub_total: 478_456_895,
            total: 0,
        }, &connection).unwrap();

        assert_eq!(
            serde_json::to_string(&list_sales(&connection)).unwrap(),
            serde_json::to_string(&vec![
                (
                    Sale {
                        id: 1,
                        date: "01/01/2020".to_string(),
                        tax_total: 210_000,
                        sub_total: 478_456_895,
                        total: 478_666_895,
                    }
                ),
            ]).unwrap()
        );

        Ok(())
    });
}


fn create_sale_test1(new_sale: NewSale, expected_sale: Sale) -> Result<bool> {
    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        create_sale(new_sale, &connection)?;

        assert_eq!(
            serde_json::to_string(&list_sales(&connection)).unwrap(),
            serde_json::to_string(&vec![(expected_sale)]).unwrap()
        );

        Ok(())
    });
    Ok(true)
}

fn create_sale_test2(new_sale: NewSale) -> Sale {
    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        create_sale(new_sale, &connection)?;
        Ok(list_sales(&connection).first().unwrap().clone())
    })
}
