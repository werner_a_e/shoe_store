#[macro_use]
extern crate diesel;

use actix_cors::Cors;
use actix_web::{web, App, HttpServer, Responder, HttpResponse};
use product::create_product;
use diesel::sqlite::SqliteConnection;
use ::shoe_store::models::NewCompleteProduct;
use ::shoe_store::establish_connection;
use anyhow::Result;

async fn product_create(product: web::Json<NewCompleteProduct>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match create_product(product.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
		let cors = Cors::permissive();

        App::new()
			.wrap(cors)
			.data(establish_connection())
            .route("products", web::post().to(product_create))
    })
    .bind(("127.0.0.1", 9000))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App};
    use actix_web::web::Bytes; 
	use diesel::result::Error;
	use diesel::Connection;
	use ::shoe_store::establish_connection_test;
	use ::shoe_store::models::{NewCompleteProduct, NewProduct, NewVariantValue, NewVariant};

    #[actix_rt::test]
    async fn test_product_creation_is_ok() {
		let connection = establish_connection_test();
		connection.begin_test_transaction().unwrap();

		let mut app = test::init_service(
			App::new()
				.data(connection)
				.route("products", web::post().to(product_create))
		).await;

		let body =
			NewCompleteProduct {
				product: NewProduct {
					name: "boots".to_string(),
					cost: 13.23,
					active: true
				},
				variants: vec![
					NewVariantValue {
						variant: NewVariant {
							name: "size".to_string()
						},
						values: vec![
							Some(12.to_string()),
							Some(14.to_string()),
							Some(16.to_string()),
							Some(18.to_string())
						]
					}
				]
			};

		let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());
    }
}

/*************************************************************************************/

mod product;
use actix_web::{web, App, HttpServer, Responder, HttpResponse};
use diesel::sqlite::SqliteConnection;
use product::list_products;
use ::shoe_store::models::*;
use ::shoe_store::establish_connection;

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App, body::Body};
    use actix_web::web::Bytes; 

	use diesel::result::Error;
	use diesel::Connection;
	use ::shoe_store::establish_connection_test;
	use ::shoe_store::models::{NewCompleteProduct, NewProduct, NewVariantValue, NewVariant};

    #[actix_rt::test]
    async fn test_product_list() {
		let connection = establish_connection_test();
		connection.begin_test_transaction().unwrap();

		let mut app = test::init_service(
			App::new()
				.data(connection)
				.route("products", web::post().to(product_create))
				.route("products", web::get().to(product_list))
		).await;

        let shoes = vec![("Boots", 14.00), ("High Heels", 19.23), ("Running Shoes", 21.90),
                         ("Tennis Shoes", 15.67), ("Hiking Boots", 18.72), ("Flip Flops", 10.5)];

        for shoe in shoes {
            let body =
                NewCompleteProduct {
                    product: NewProduct {
                        name: shoe.0.to_string(),
                        cost: shoe.1,
                        active: true
                    },
                    variants: vec![
                        NewVariantValue {
                            variant: NewVariant {
                                name: "size".to_string()
                            },
                            values: vec![
                                Some(12.to_string()),
                                Some(14.to_string()),
                                Some(16.to_string()),
                                Some(18.to_string())
                            ]
                        }
                    ]
                };

            let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
            let resp = test::call_service(&mut app, req).await;

            assert!(resp.status().is_success());
        }

        let req = test::TestRequest::get().uri("/products?limit=5").to_request();
        let mut resp = test::call_service(&mut app, req).await;

        let body = resp.take_body();
        let body = body.as_ref().unwrap();
        assert_eq!(
            &Body::from(
                "[[{\"id\":1,\"name\":\"Boots\",\"cost\":14.0,\"active\":true},[[{\"id\":1,\"variant_id\":1,\"product_id\":1,\"value\":\"12\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":2,\"variant_id\":1,\"product_id\":1,\"value\":\"14\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":3,\"variant_id\":1,\"product_id\":1,\"value\":\"16\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":4,\"variant_id\":1,\"product_id\":1,\"value\":\"18\"},{\"id\":1,\"name\":\"size\"}]]],[{\"id\":2,\"name\":\"High Heels\",\"cost\":19.23,\"active\":true},[[{\"id\":5,\"variant_id\":1,\"product_id\":2,\"value\":\"12\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":6,\"variant_id\":1,\"product_id\":2,\"value\":\"14\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":7,\"variant_id\":1,\"product_id\":2,\"value\":\"16\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":8,\"variant_id\":1,\"product_id\":2,\"value\":\"18\"},{\"id\":1,\"name\":\"size\"}]]],[{\"id\":3,\"name\":\"Running Shoes\",\"cost\":21.9,\"active\":true},[[{\"id\":9,\"variant_id\":1,\"product_id\":3,\"value\":\"12\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":10,\"variant_id\":1,\"product_id\":3,\"value\":\"14\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":11,\"variant_id\":1,\"product_id\":3,\"value\":\"16\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":12,\"variant_id\":1,\"product_id\":3,\"value\":\"18\"},{\"id\":1,\"name\":\"size\"}]]],[{\"id\":4,\"name\":\"Tennis Shoes\",\"cost\":15.67,\"active\":true},[[{\"id\":13,\"variant_id\":1,\"product_id\":4,\"value\":\"12\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":14,\"variant_id\":1,\"product_id\":4,\"value\":\"14\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":15,\"variant_id\":1,\"product_id\":4,\"value\":\"16\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":16,\"variant_id\":1,\"product_id\":4,\"value\":\"18\"},{\"id\":1,\"name\":\"size\"}]]],[{\"id\":5,\"name\":\"Hiking Boots\",\"cost\":18.72,\"active\":true},[[{\"id\":17,\"variant_id\":1,\"product_id\":5,\"value\":\"12\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":18,\"variant_id\":1,\"product_id\":5,\"value\":\"14\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":19,\"variant_id\":1,\"product_id\":5,\"value\":\"16\"},{\"id\":1,\"name\":\"size\"}],[{\"id\":20,\"variant_id\":1,\"product_id\":5,\"value\":\"18\"},{\"id\":1,\"name\":\"size\"}]]]]"
            ),
            body
        );
    }
}

/********************************************************************************************************* */

use actix_web::{web, Responder, HttpResponse,HttpServer,App};
use diesel::sqlite::SqliteConnection;
use diesel::{RunQueryDsl, QueryDsl, Connection};
use anyhow::Result;

async fn product_update(id: web::Path<i32>, product: web::Json<FormProduct>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match update_product(*id, product.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

use ::shoe_store::establish_connection;
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
			.data(establish_connection())
            .route("products", web::put().to(product_update))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App, body::Body};
	use diesel::Connection;
	use ::shoe_store::establish_connection_test;
	use ::shoe_store::models::{NewCompleteProduct, NewProduct, NewVariantValue, NewVariant};

    #[actix_rt::test]
    async fn test_product_creation_is_ok() {
		let connection = establish_connection_test();
		connection.begin_test_transaction().unwrap();

		let mut app = test::init_service(
			App::new()
				.data(connection)
				.route("products", web::post().to(product_create))
				.route("products/{id}", web::put().to(product_update))
                .route("products/{id}", web::get().to(product_show))
		).await;

		let body =
			NewCompleteProduct {
				product: NewProduct {
					name: "boots".to_string(),
					cost: 13.23,
					active: true
				},
				variants: vec![
					NewVariantValue {
						variant: NewVariant {
							name: "size".to_string()
						},
						values: vec![
							Some(12.to_string()),
							Some(14.to_string()),
							Some(16.to_string()),
							Some(18.to_string())
						]
					}
				]
			};

		let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

        let body =
            FormProduct {
                product: NewProduct {
                    name: "high heels".to_string(),
                    cost: 15.00,
                    active: true
                },
                variants: vec![
                    FormProductVariantComplete {
                        variant: Some(FormVariant {
                            id: Some(1),
                            name: "size".to_string()
                        }),
                        product_variant: FormProductVariant {
                            id: Some(1),
                            variant_id: Some(1),
                            product_id: 1,
                            value: Some(20.to_string())
                        }
                    }
                ]
            };
        let req = test::TestRequest::put().set_json(&body).uri("/products/1").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

        let req = test::TestRequest::get().uri("/products/1").to_request();
        let mut resp = test::call_service(&mut app, req).await;

        let body = resp.take_body();
        let body = body.as_ref().unwrap();

        let result = (Product {
            id: 1,
            name: "high heels".to_string(),
            cost: 15.00,
            active: true
        }, vec![
            (ProductVariant {
                id: 1,
                variant_id: 1,
                product_id: 1,
                value: Some(20.to_string())
            },
            Variant {
                id: 1,
                name: "size".to_string()
            }),
            (ProductVariant {
                id: 2,
                variant_id: 1,
                product_id: 1,
                value: Some(14.to_string())
            },
            Variant {
                id: 1,
                name: "size".to_string()
            }),
            (ProductVariant {
                id: 3,
                variant_id: 1,
                product_id: 1,
                value: Some(16.to_string())
            },
            Variant {
                id: 1,
                name: "size".to_string()
            }),
            (ProductVariant {
                id: 4,
                variant_id: 1,
                product_id: 1,
                value: Some(18.to_string())
            },
            Variant {
                id: 1,
                name: "size".to_string()
            })
        ]);
        assert_eq!(
            &Body::from(
                serde_json::to_string(&result).unwrap()
            ),
            body
        );
    }
}


/*************************HIDDEN***************************** */
use ::shoe_store::schema::products_variants;
use serde::{Serialize, Deserialize};
use ::shoe_store::models::{FormVariant, NewProduct};

#[derive(Insertable, Debug, AsChangeset, Clone, Serialize, Deserialize)]
#[table_name="products_variants"]
pub struct FormProductVariant {
    pub id: Option<i32>,
    pub variant_id: Option<i32>,
    pub product_id: i32,
    pub value: Option<String>
}

#[derive(Clone, Serialize,  Deserialize)]
pub struct FormProductVariantComplete {
    pub variant: Option<FormVariant>,
    pub product_variant: FormProductVariant,
}
#[derive(Clone, Serialize, Deserialize)]
pub struct FormProduct {
    pub product: NewProduct,
    pub variants: Vec<FormProductVariantComplete>
}

no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

fn update_product(product_id: i32, form_product: FormProduct, conn: &SqliteConnection) -> Result<i32> {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants;
    use ::shoe_store::schema::products_variants::dsl::products_variants;

    conn.transaction(|| {
        diesel::update(products.find(product_id))
            .set(&form_product.product)
            .execute(conn)?;

        for mut form_product_variant in form_product.variants {
            if form_product_variant.product_variant.variant_id.is_none() {
                diesel::insert_into(variants::dsl::variants)
                    .values(form_product_variant.variant)
                    .execute(conn)?;

                let last_variant_id: i32 =
                        diesel::select(last_insert_rowid).first(conn)?;

                form_product_variant.product_variant.variant_id = Some(last_variant_id);
            }
            if let Some(product_variant_id) = form_product_variant.product_variant.id {
                diesel::update(products_variants.find(product_variant_id))
                    .set(&form_product_variant.product_variant)
                    .execute(conn)?;
            } else {
                diesel::insert_into(products_variants)
                    .values(&form_product_variant.product_variant)
                    .execute(conn)?;
            }
        }

        Ok(product_id)
    })
}

use ::shoe_store::models::{NewCompleteProduct, Variant};

async fn product_create(product: web::Json<NewCompleteProduct>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match create_product(product.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}
use diesel::{ExpressionMethods, RunQueryDsl, Connection, QueryDsl};

pub fn create_product(new_product: NewCompleteProduct, conn: &SqliteConnection) -> Result<i32>  {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::*;
    use ::shoe_store::schema::products_variants::dsl::*;

    conn.transaction(|| {
        diesel::insert_into(products)
            .values(new_product.product)
            .execute(conn)?;

        let last_product_id: i32 = diesel::select(last_insert_rowid).first(conn)?;

        for new_variant in new_product.variants {
            let variants_result =
                variants
                    .filter(name.eq(&new_variant.variant.name))
                    .limit(1)
                    .load::<Variant>(conn)?;

            let last_variant_id: i32 =
                match variants_result.first() {
                    Some(variant) => variant.id,
                    None => {
                        diesel::insert_into(variants)
                            .values(name.eq(&new_variant.variant.name))
                            .execute(conn)?;

                        diesel::select(last_insert_rowid).first(conn)?
                    }
                };

            for new_value in new_variant.values {
                diesel::insert_into(products_variants)
                    .values(
                        (
                            product_id.eq(last_product_id), 
                            variant_id.eq(last_variant_id),
                            value.eq(new_value), 
                        )
                    )
                    .execute(conn)?;
            }
        }
        Ok(last_product_id)
    })
}

/****************************************************************************///

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App, body::Body};
	use diesel::Connection;
	use ::shoe_store::establish_connection_test;
	use ::shoe_store::models::NewCompleteProduct;

    #[actix_rt::test]
    async fn test_product_searching() {
		let connection = establish_connection_test();
		connection.begin_test_transaction().unwrap();

		let mut app = test::init_service(
			App::new()
				.data(connection)
				.route("products", web::post().to(product_create))
				.route("products", web::get().to(product_search))
		).await;

		let body =
			NewCompleteProduct {
				product: NewProduct {
					name: "Boots".to_string(),
					cost: 13.23,
					active: true
				},
				variants: vec![
					NewVariantValue {
						variant: NewVariant {
							name: "size".to_string()
						},
						values: vec![
							Some(12.to_string()),
							Some(14.to_string()),
							Some(16.to_string()),
							Some(18.to_string())
						]
					}
				]
			};

		let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

		let body =
			NewCompleteProduct {
				product: NewProduct {
					name: "Sandals".to_string(),
					cost: 15.00,
					active: true
				},
				variants: vec![
					NewVariantValue {
						variant: NewVariant {
							name: "size".to_string()
						},
						values: vec![
							Some(20.to_string()),
						]
					}
				]
			};

		let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

        let req = test::TestRequest::get().uri("/products?search=Sandals").to_request();
        let mut resp = test::call_service(&mut app, req).await;

        let body = resp.take_body();
        let body = body.as_ref().unwrap();

        let result = vec![(Product {
            id: 2,
			name: "Sandals".to_string(),
			cost: 15.00,
            active: true
        }, vec![
            (ProductVariant {
                id: 5,
                variant_id: 1,
                product_id: 2,
                value: Some(20.to_string())
            },
            Variant {
                id: 1,
                name: "size".to_string()
            })
        ])];
        assert_eq!(
            &Body::from(
                serde_json::to_string(&result).unwrap()
            ),
            body
        );
    }
}


/*******************************************************Hidden***************************************/
use serde::Deserialize;
use actix_web::{web, App, HttpServer, Responder, HttpResponse};
use diesel::{TextExpressionMethods, QueryDsl, RunQueryDsl};
use diesel::sqlite::SqliteConnection;
use ::shoe_store::establish_connection;
use ::shoe_store::models::{Product, ProductVariant, Variant};
use diesel::result::Error;
use ::shoe_store::models::{NewCompleteProduct, NewProduct, NewVariantValue, NewVariant};
use anyhow::Result;

#[derive(Deserialize, Clone)]
struct QueryParamsSearch {
	search: String
}

async fn product_search(search_param: web::Query<QueryParamsSearch>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
		match search_products(search_param.search.clone(), &conn) {
			Ok(products) => Ok(web::Json(products)),
			Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
		}
	}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
			.data(establish_connection())
            .route("products", web::get().to(product_search))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}

use crate::diesel::GroupedBy;
use crate::diesel::BelongingToDsl;
fn search_products(search: String, conn: &SqliteConnection) -> Result<Vec<(Product, Vec<(ProductVariant, Variant)>)>, Error> {
    use ::shoe_store::schema::products::dsl::*;
    use ::shoe_store::schema::variants::dsl::variants;

    let pattern = format!("%{}%", search);
    let products_result = 
        products
        .filter(name.like(pattern))
        .load::<Product>(conn)?;
    let variants_result =
        ProductVariant::belonging_to(&products_result)
            .inner_join(variants)
            .load::<(ProductVariant, Variant)>(conn)?
            .grouped_by(&products_result);
    let data = products_result.into_iter().zip(variants_result).collect::<Vec<_>>();

    Ok(data)
}

async fn product_create(product: web::Json<NewCompleteProduct>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match create_product(product.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

use crate::diesel::ExpressionMethods;
use crate::diesel::Connection;
no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

pub fn create_product(new_product: NewCompleteProduct, conn: &SqliteConnection) -> Result<i32>  {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::*;
    use ::shoe_store::schema::products_variants::dsl::*;

    conn.transaction(|| {
        diesel::insert_into(products)
            .values(new_product.product)
            .execute(conn)?;

        let last_product_id: i32 = diesel::select(last_insert_rowid).first(conn)?;

        for new_variant in new_product.variants {
            let variants_result =
                variants
                    .filter(name.eq(&new_variant.variant.name))
                    .limit(1)
                    .load::<Variant>(conn)?;

            let last_variant_id: i32 =
                match variants_result.first() {
                    Some(variant) => variant.id,
                    None => {
                        diesel::insert_into(variants)
                            .values(name.eq(&new_variant.variant.name))
                            .execute(conn)?;

                        diesel::select(last_insert_rowid).first(conn)?
                    }
                };

            for new_value in new_variant.values {
                diesel::insert_into(products_variants)
                    .values(
                        (
                            product_id.eq(last_product_id), 
                            variant_id.eq(last_variant_id),
                            value.eq(new_value), 
                        )
                    )
                    .execute(conn)?;
            }
        }
        Ok(last_product_id)
    })
}

/*************************************************************************************************************/

async fn product_delete(id: web::Path<i32>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
		match delete_product(*id, &conn) {
			Ok(_) => Ok(HttpResponse::Ok()),
			Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
		}
	}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
			.data(establish_connection())
            .route("products/{id}", web::delete().to(product_delete))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App, body::Body};
	use diesel::Connection;
	use ::shoe_store::establish_connection_test;
	use ::shoe_store::models::{Product, NewCompleteProduct, NewProduct, NewVariantValue, NewVariant};

    #[actix_rt::test]
    async fn test_product_delete_is_ok() {
		let connection = establish_connection_test();
		connection.begin_test_transaction().unwrap();

		let mut app = test::init_service(
			App::new()
				.data(connection)
				.route("products", web::post().to(product_create))
				.route("products/{id}", web::delete().to(product_delete))
				.route("products", web::get().to(product_list))
		).await;

		let body =
			NewCompleteProduct {
				product: NewProduct {
					name: "boots".to_string(),
					cost: 13.23,
					active: true
				},
				variants: vec![
					NewVariantValue {
						variant: NewVariant {
							name: "size".to_string()
						},
						values: vec![
							Some(12.to_string()),
						]
					}
				]
			};

		let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

        let req = test::TestRequest::get().uri("/products?limit=5").to_request();
        let mut resp = test::call_service(&mut app, req).await;

        let body = resp.take_body();
        let body = body.as_ref().unwrap();

        let result = vec![(Product {
            id: 1,
            name: "boots".to_string(),
            cost: 13.23,
            active: true
        }, vec![
            (ProductVariant {
                id: 1,
                variant_id: 1,
                product_id: 1,
                value: Some(12.to_string())
            },
            Variant {
                id: 1,
                name: "size".to_string()
            })
		])];

        assert_eq!(
            &Body::from(serde_json::to_string(&result).unwrap()),
            body
        );

        let req = test::TestRequest::delete().uri("/products/1").to_request();
        let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

        let req = test::TestRequest::get().uri("/products?limit=5").to_request();
        let mut resp = test::call_service(&mut app, req).await;

        let body = resp.take_body();
        let body = body.as_ref().unwrap();

        let result: Vec<Product> = vec![];

        assert_eq!(
            &Body::from(serde_json::to_string(&result).unwrap()),
            body
        );
    }
}

/**********************************************************************************************************/

use actix_web::{web, App, HttpRequest, HttpServer, Responder, Result, middleware::Logger};
use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize)]
struct Person {
	name: String,
	age: i32
}

async fn greet(req: HttpRequest) -> Result<impl Responder> {
    let name = req.match_info().get("name").unwrap_or("World");
	if name == "Error" {
		Err(actix_web::error::ErrorInternalServerError("an error"))
	} else {
		Ok(web::Json(Person { name: "Peter Parker".to_string(), age: 32 }))
	}
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
	env_logger::init_from_env(env_logger::Env::new().default_filter_or("info"));
    HttpServer::new(|| {
        App::new()
			.wrap(Logger::new("%a %{User-Agent}i"))
            .route("/{name}", web::get().to(greet))
    })
    .bind(("127.0.0.1", 8080))?
    .run()
    .await
}
/************************************************************************************** */

#[cfg(test)]
mod tests {
    use super::*;
    use actix_web::{test, web, App};
	use actix_web::body::{Body, ResponseBody};
	use diesel::Connection;
	use ::shoe_store::establish_connection_test;
	use ::shoe_store::models::{NewCompleteProduct, NewProduct, NewVariantValue, NewVariant};

    trait BodyTest {
        fn as_str(&self) -> &str;
    }

    impl BodyTest for ResponseBody<Body> {
        fn as_str(&self) -> &str {
            match self {
                ResponseBody::Body(ref b) => match b {
                    Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                    _ => panic!(),
                },
                ResponseBody::Other(ref b) => match b {
                    Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                    _ => panic!(),
                },
            }
        }
    }

    #[actix_rt::test]
    async fn test_product_creation_is_ok() {
		let connection = establish_connection_test();
		connection.begin_test_transaction().unwrap();

		let mut app = test::init_service(
			App::new()
				.data(connection)
				.route("products", web::post().to(product_create))
                .route("products/{id}", web::get().to(product_show))
		).await;

		let body =
			NewCompleteProduct {
				product: NewProduct {
					name: "boots".to_string(),
					cost: 13.23,
					active: true
				},
				variants: vec![
					NewVariantValue {
						variant: NewVariant {
							name: "size".to_string()
						},
						values: vec![
							Some(12.to_string()),
							Some(14.to_string()),
							Some(16.to_string()),
							Some(18.to_string())
						]
					}
				]
			};

		let req = test::TestRequest::post().set_json(&body).uri("/products").to_request();
		let resp = test::call_service(&mut app, req).await;

		assert!(resp.status().is_success());

        let req = test::TestRequest::get().uri("/products/1").to_request();
        let mut resp = test::call_service(&mut app, req).await;

		println!("{:#?}", resp.take_body().as_str());

    }
}