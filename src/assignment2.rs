#[macro_use]
extern crate diesel;
extern crate anyhow;

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use serde::{Serialize, Deserialize};
use actix_web::{test, web, App};
use diesel::sqlite::SqliteConnection;
use diesel::Connection;
use ::shoe_store::establish_connection_test;
use diesel::result::Error;
use anyhow::Result;
use actix_web::body::{Body, ResponseBody};

#[derive(Identifiable, Queryable, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[table_name = "sales"]
pub struct Sale {
    pub id: i32,
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

#[actix_web::main]
async fn main() {
    let connection = establish_connection_test();
    connection.begin_test_transaction().unwrap();

    let mut app = test::init_service(
        App::new()
            .data(connection)
            .route("sales", web::post().to(sale_create))
            .route("sales/{id}", web::get().to(sale_show))
    ).await;

    let mut sale_id = 1;

    if let Ok(lines) = read_lines("inputs.txt") {
        for line in lines {
            if let Ok(new_sale_str) = line {
                let new_sale: NewSale = serde_json::from_str(&new_sale_str).unwrap();                
                
                let req = test::TestRequest::post().set_json(&new_sale).uri("/sales").to_request();
                let resp = test::call_service(&mut app, req).await;

		        assert!(resp.status().is_success());

                let req = test::TestRequest::get().uri(&format!("/sales/{}", sale_id)).to_request();
                let mut resp = test::call_service(&mut app, req).await;

                println!("{:#?}", resp.take_body().as_str());
            }
            sale_id += 1;
        }
    }
}

fn create_sale(new_sale: NewSale, conn: &SqliteConnection) -> Result<usize, Error> {    
    use ::shoe_store::schema::sales::dsl::*;
    diesel::insert_into(sales)
        .values(NewSale {
            date: new_sale.date,
            tax_total: new_sale.tax_total,
            sub_total: new_sale.sub_total,
            total: new_sale.tax_total + new_sale.sub_total
        })
        .execute(conn)
}

use diesel::{QueryDsl, RunQueryDsl};

fn show_sale(sale_id: i32, conn: &SqliteConnection) -> Result<Sale, Error> {
    sales::dsl::sales
        .find(sale_id)
        .first::<Sale>(conn)
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

trait BodyTest {
    fn as_str(&self) -> &str;
}

impl BodyTest for ResponseBody<Body> {
    fn as_str(&self) -> &str {
        match self {
            ResponseBody::Body(ref b) => match b {
                Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                _ => panic!(),
            },
            ResponseBody::Other(ref b) => match b {
                Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                _ => panic!(),
            },
        }
    }
}

use actix_web::{Responder, HttpResponse};
use shoe_store::schema::sales;

#[derive(Insertable, Debug, AsChangeset, Clone, Serialize, Deserialize)]
#[table_name="sales"]
pub struct NewSale {
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

async fn sale_create(sale: web::Json<NewSale>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match create_sale(sale.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

async fn sale_show(id: web::Path<i32>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match show_sale(*id, &conn) {
		Ok(sale) => Ok(web::Json(sale)),
		Err(error) => Err(actix_web::error::ErrorNotFound(error))
	}
}
/*****************************************************************************************************************/
#[macro_use]
extern crate diesel;
use shoe_store::schema::sales;
use serde::{Serialize, Deserialize};

#[derive(Insertable, Debug, AsChangeset, Serialize, Deserialize, Clone)]
#[table_name="sales"]
pub struct FormSale {
    pub date: String,
    pub tax_total: Option<i32>,
    pub sub_total: Option<i32>,
    pub total: Option<i32>
}

async fn sale_update(id: web::Path<i32>, sale: web::Json<FormSale>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match update_sale(*id, sale.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

async fn sale_delete(id: web::Path<i32>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match delete_sale(*id, &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))        
	}
}

extern crate anyhow;

use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use actix_web::{test, web, App, Responder, HttpResponse};
use diesel::sqlite::SqliteConnection;
use diesel::Connection;
use ::shoe_store::establish_connection_test;
use diesel::result::Error;
use anyhow::Result;
use actix_web::body::{Body, ResponseBody};

#[derive(Identifiable, Queryable, Clone, Debug, PartialEq, Serialize, Deserialize)]
#[table_name = "sales"]
pub struct Sale {
    pub id: i32,
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

#[derive(Insertable, Debug, AsChangeset, Clone, Serialize, Deserialize)]
#[table_name="sales"]
pub struct NewSale {
    pub date: String,
    pub tax_total: i32,
    pub sub_total: i32,
    pub total: i32
}

no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

#[actix_web::main]
async fn main() {
    let mut count_for_deletion = 1;
    let mut count_for_date = 1;
    let mut sale_id = 1;

    let connection = establish_connection_test();
    connection.begin_test_transaction().unwrap();

    let mut app = test::init_service(
        App::new()
            .data(connection)
            .route("sales", web::post().to(sale_create))
            .route("sales/{id}", web::put().to(sale_update))
            .route("sales/{id}", web::put().to(sale_delete))
            .route("sales/{id}", web::get().to(sale_show))
    ).await;

    if let Ok(lines) = read_lines("inputs_for_update.txt") {
        for line in lines {
            if let Ok(new_sale_str) = line {
                let new_sale: NewSale = serde_json::from_str(&new_sale_str).unwrap();         
                
                let req = test::TestRequest::post().set_json(&new_sale).uri("/sales").to_request();
                let resp = test::call_service(&mut app, req).await;

		        assert!(resp.status().is_success());

                let sale_for_update =
                 FormSale {
                        date: format!("0{}/01/2020", count_for_date).to_string(),
                        tax_total: None,
                        sub_total: None,
                        total: None
                    };

                let req = test::TestRequest::put().set_json(&sale_for_update).uri(&format!("/sales/{}", sale_id)).to_request();
                let _resp = test::call_service(&mut app, req).await;

		        assert!(resp.status().is_success());

                if count_for_deletion % 2 == 0 {
                    let req = test::TestRequest::delete().uri(&format!("/sales/{}", sale_id)).to_request();
                    let _resp = test::call_service(&mut app, req).await;
    		        assert!(resp.status().is_success());
                } else {
                    let req = test::TestRequest::get().uri(&format!("/sales/{}", sale_id)).to_request();
                    let mut resp = test::call_service(&mut app, req).await;

                    println!("{:#?}", resp.take_body().as_str());
                    count_for_date += 1;
                }

            }
            count_for_deletion += 1;
            sale_id += 1;
        }
    }
}

async fn sale_create(sale: web::Json<NewSale>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match create_sale(sale.clone(), &conn) {
		Ok(_) => Ok(HttpResponse::Ok()),
		Err(error) => Err(actix_web::error::ErrorInternalServerError(error))
	}
}

async fn sale_show(id: web::Path<i32>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match show_sale(*id, &conn) {
		Ok(sale) => Ok(web::Json(sale)),
		Err(error) => Err(actix_web::error::ErrorNotFound(error))
	}
}

fn create_sale(new_sale: NewSale, conn: &SqliteConnection) -> Result<usize, Error> {    
    use ::shoe_store::schema::sales::dsl::*;
    diesel::insert_into(sales)
        .values(NewSale {
            date: new_sale.date,
            tax_total: new_sale.tax_total,
            sub_total: new_sale.sub_total,
            total: new_sale.tax_total + new_sale.sub_total
        })
        .execute(conn)
}

use diesel::{QueryDsl, RunQueryDsl};

fn show_sale(sale_id: i32, conn: &SqliteConnection) -> Result<Sale, Error> {
    sales::dsl::sales
        .find(sale_id)
        .first::<Sale>(conn)
}

fn update_sale(sale_id: i32, form_sale: FormSale, conn: &SqliteConnection) -> Result<usize, Error> {
    use ::shoe_store::schema::sales::dsl::sales;

    diesel::update(sales.find(sale_id))
        .set(&form_sale)
        .execute(conn)
}

fn delete_sale(sale_id: i32, conn:  &SqliteConnection) -> Result<usize, Error> {
    use ::shoe_store::schema::sales::dsl::sales;
    diesel::delete(sales.find(sale_id))
        .execute(conn)
}

fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where P: AsRef<Path>, {
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

trait BodyTest {
    fn as_str(&self) -> &str;
}

impl BodyTest for ResponseBody<Body> {
    fn as_str(&self) -> &str {
        match self {
            ResponseBody::Body(ref b) => match b {
                Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                _ => panic!(),
            },
            ResponseBody::Other(ref b) => match b {
                Body::Bytes(ref by) => std::str::from_utf8(&by).unwrap(),
                _ => panic!(),
            },
        }
    }
}