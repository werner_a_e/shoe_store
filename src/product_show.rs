use actix_web::{web, App, HttpServer, Responder};
use diesel::sqlite::SqliteConnection;
use serde::Deserialize;
use ::shoe_store::models::*;
use anyhow::Result;
use diesel::result::Error;
use actix_cors::Cors;
use diesel::{RunQueryDsl, QueryDsl};

async fn product_show(id: web::Path<i32>, conn: web::Data<SqliteConnection>)
	-> actix_web::Result<impl Responder> {
	match show_product(*id, &conn) {
		Ok(product) => Ok(web::Json(product)),
		Err(error) => Err(actix_web::error::ErrorNotFound(error))
	}
}

use ::shoe_store::models::{Product, ProductVariant};
use diesel::BelongingToDsl;
fn show_product(id: i32, conn: &SqliteConnection) -> Result<(Product, Vec<(ProductVariant, Variant)>), diesel::result::Error> {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::variants;

    let product_result =
        products
            .find(id)
            .get_result::<Product>(conn)?;

    let variants_result =
        ProductVariant::belonging_to(&product_result)
            .inner_join(variants)
            .load::<(ProductVariant, Variant)>(conn)?;

    Ok((product_result, variants_result))
}


use ::shoe_store::establish_connection;
#[actix_web::main]
async fn main() -> std::io::Result<()> {

    HttpServer::new(|| {
        let cors = Cors::permissive();
        App::new()
			.wrap(cors)
			.data(establish_connection())
            .route("products/{id}", web::get().to(product_show))
    })
    .bind(("127.0.0.1", 9400))?
    .run()
    .await
}

