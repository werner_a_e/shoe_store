use diesel::query_dsl::QueryDsl;
use diesel::RunQueryDsl;
use ::shoe_store::models::*;

use anyhow::Result;
use diesel::BelongingToDsl;
#[cfg(test)]
use diesel::debug_query;
#[cfg(test)]
use diesel::sqlite::Sqlite;
use crate::diesel::GroupedBy;

pub fn list_products(conn: &SqliteConnection) -> Result<Vec<(Product, Vec<(ProductVariant, Variant)>)>, Error> {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::variants;

    let products_result = 
        products
        .limit(10)
        .load::<Product>(conn)?;
    let variants_result =
        ProductVariant::belonging_to(&products_result)
            .inner_join(variants)
            .load::<(ProductVariant, Variant)>(conn)?
            .grouped_by(&products_result);
    let data = products_result.into_iter().zip(variants_result).collect::<Vec<_>>();

    Ok(data)
}

use diesel::result::Error;
use diesel::Connection;
use ::shoe_store::establish_connection_test;

#[test]
fn test_list_products() {
    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        let variants = vec![
            NewVariantValue {
                variant: NewVariant {
                    name: "size".to_string()
                },
                values: vec![
                    Some(12.to_string()),
                    Some(14.to_string()),
                    Some(16.to_string()),
                    Some(18.to_string())
                ]
            }
        ];

        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "boots".to_string(),
                cost: 13.23,
                active: true
            },
            variants: variants.clone()
        }, &connection).unwrap();
        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "high heels".to_string(),
                cost: 20.99,
                active: true
            },
            variants: variants.clone()
        }, &connection).unwrap();
        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "running shoes".to_string(),
                cost: 10.99,
                active: true
            },
            variants: variants.clone()
        }, &connection).unwrap();

        let variants_result = |start_id, for_product_id| {
            vec![
                (
                    ProductVariant {
                        id: start_id + 1,
                        variant_id: 1,
                        product_id: for_product_id,
                        value: Some(
                            "12".to_string(),
                        ),
                    },
                    Variant {
                        id: 1,
                        name: "size".to_string(),
                    }
                ),
                (
                    ProductVariant {
                        id: start_id + 2,
                        variant_id: 1,
                        product_id: for_product_id,
                        value: Some(
                            "14".to_string(),
                        ),
                    },
                    Variant {
                        id: 1,
                        name: "size".to_string(),
                    }
                ),
                (
                    ProductVariant {
                        id: start_id + 3,
                        variant_id: 1,
                        product_id: for_product_id,
                        value: Some(
                            "16".to_string(),
                        ),
                    },
                    Variant {
                        id: 1,
                        name: "size".to_string(),
                    }
                ),
                (
                    ProductVariant {
                        id: start_id + 4,
                        variant_id: 1,
                        product_id: for_product_id,
                        value: Some(
                            "18".to_string(),
                        ),
                    },
                    Variant {
                        id: 1,
                        name: "size".to_string(),
                    }
                )
            ]
        };

        assert_eq!(serde_json::to_string(&list_products(&connection).unwrap()).unwrap(), serde_json::to_string(&vec![
            (
                Product {
                    id: 1,
                    name: "boots".to_string(),
                    cost: 13.23,
                    active: true
                },
                variants_result(0, 1)
            ),
            (
                Product {
                    id: 2,
                    name: "high heels".to_string(),
                    cost: 20.99,
                    active: true
                },
                variants_result(4, 2)
            ),
            (
                Product {
                    id: 3,
                    name: "running shoes".to_string(),
                    cost: 10.99,
                    active: true
                },
                variants_result(8, 3)
            )
        ]).unwrap());

        Ok(())

    });
}

use diesel::sqlite::SqliteConnection;
use crate::diesel::ExpressionMethods;

no_arg_sql_function!(last_insert_rowid, diesel::sql_types::Integer);

pub fn create_product(new_product: NewCompleteProduct, conn: &SqliteConnection) -> Result<i32>  {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::*;
    use ::shoe_store::schema::products_variants::dsl::*;

    conn.transaction(|| {
        diesel::insert_into(products)
            .values(new_product.product)
            .execute(conn)?;

        let last_product_id: i32 = diesel::select(last_insert_rowid).first(conn)?;

        for new_variant in new_product.variants {
            let variants_result =
                variants
                    .filter(name.eq(&new_variant.variant.name))
                    .limit(1)
                    .load::<Variant>(conn)?;

            let last_variant_id: i32 =
                match variants_result.first() {
                    Some(variant) => variant.id,
                    None => {
                        diesel::insert_into(variants)
                            .values(name.eq(&new_variant.variant.name))
                            .execute(conn)?;

                        diesel::select(last_insert_rowid).first(conn)?
                    }
                };

            for new_value in new_variant.values {
                diesel::insert_into(products_variants)
                    .values(
                        (
                            product_id.eq(last_product_id), 
                            variant_id.eq(last_variant_id),
                            value.eq(new_value), 
                        )
                    )
                    .execute(conn)?;
            }
        }
        Ok(last_product_id)
    })
}

#[test]
fn create_product_test() {
    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "boots".to_string(),
                cost: 13.23,
                active: true
            },
            variants: vec![
                NewVariantValue {
                    variant: NewVariant {
                        name: "size".to_string()
                    },
                    values: vec![
                        Some(12.to_string()),
                        Some(14.to_string()),
                        Some(16.to_string()),
                        Some(18.to_string())
                    ]
                }
            ]
        }, &connection).unwrap();

        assert_eq!(
            serde_json::to_string(&list_products(&connection).unwrap()).unwrap(),
            serde_json::to_string(&vec![
                (
                    Product {
                        id: 1,
                        name: "boots".to_string(),
                        cost: 13.23,
                        active: true
                    },
                    vec![
                        (
                            ProductVariant {
                                id: 1,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "12".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 2,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "14".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 3,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "16".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 4,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "18".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        )
                    ]
                ),
            ]).unwrap()
        );

        Ok(())
    });
}

fn show_product(id: i32, conn: &SqliteConnection) -> Result<(Product, Vec<(ProductVariant, Variant)>), Error> {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants::dsl::variants;

    let product_result =
        products
            .find(id)
            .get_result::<Product>(conn)?;

    let variants_result =
        ProductVariant::belonging_to(&product_result)
            .inner_join(variants)
            .load::<(ProductVariant, Variant)>(conn)?;

    Ok((product_result, variants_result))
}

#[test]
fn show_product_test() {

    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        let product_id =
            create_product(NewCompleteProduct {
                product: NewProduct {
                    name: "boots".to_string(),
                    cost: 13.23,
                    active: true
                },
                variants: vec![
                    NewVariantValue {
                        variant: NewVariant {
                            name: "size".to_string()
                        },
                        values: vec![
                            Some(12.to_string()),
                            Some(14.to_string()),
                            Some(16.to_string()),
                            Some(18.to_string())
                        ]
                    }
                ]
            }, &connection).unwrap();

        assert_eq!(
            serde_json::to_string(&show_product(product_id, &connection).unwrap()).unwrap(),
            serde_json::to_string(
                &(
                    Product {
                        id: 1,
                        name: "boots".to_string(),
                        cost: 13.23,
                        active: true
                    },
                    vec![
                        (
                            ProductVariant {
                                id: 1,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "12".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 2,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "14".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 3,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "16".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 4,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "18".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        )
                    ]
                )
            ).unwrap()
        );

        Ok(())
    });
}

fn update_product(product_id: i32, form_product: FormProduct, conn: &SqliteConnection) -> Result<i32> {
    use ::shoe_store::schema::products::dsl::products;
    use ::shoe_store::schema::variants;
    use ::shoe_store::schema::products_variants::dsl::products_variants;

    conn.transaction(|| {
        diesel::update(products.find(product_id))
            .set(&form_product.product)
            .execute(conn)?;

        for mut form_product_variant in form_product.variants {
            if form_product_variant.product_variant.variant_id.is_none() {
                diesel::insert_into(variants::dsl::variants)
                    .values(form_product_variant.variant)
                    .execute(conn)?;

                let last_variant_id: i32 =
                        diesel::select(last_insert_rowid).first(conn)?;

                form_product_variant.product_variant.variant_id = Some(last_variant_id);
            }
            if let Some(product_variant_id) = form_product_variant.product_variant.id {
                diesel::update(products_variants.find(product_variant_id))
                    .set(&form_product_variant.product_variant)
                    .execute(conn)?;
            } else {
                diesel::insert_into(products_variants)
                    .values(&form_product_variant.product_variant)
                    .execute(conn)?;
            }
        }

        Ok(product_id)
    })
}

#[test]
fn update_product_test() {
    use ::shoe_store::schema::products_variants::dsl::*;

    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        let created_product_id =
            create_product(NewCompleteProduct {
                product: NewProduct {
                    name: "boots".to_string(),
                    cost: 13.23,
                    active: true
                },
                variants: vec![
                    NewVariantValue {
                        variant: NewVariant {
                            name: "size".to_string()
                        },
                        values: vec![
                            Some(12.to_string()),
                            Some(14.to_string()),
                            Some(16.to_string()),
                            Some(18.to_string())
                        ]
                    }
                ]
            }, &connection).unwrap();

        let vec_product_variant =
            products_variants
                .filter(product_id.eq(created_product_id))
                .load::<ProductVariant>(&connection)
                .unwrap();

        let product_variant =
            vec_product_variant
                .first()
                .unwrap();

        update_product(
            created_product_id,
            FormProduct {
                product: NewProduct {
                    name: "high heels".to_string(),
                    cost: 14.25,
                    active: false
                },
                variants: vec![
                    FormProductVariantComplete {
                        variant: Some(FormVariant {
                            id: None,
                            name: "color".to_string()
                        }),
                        product_variant: FormProductVariant {
                            id: None,
                            product_id: created_product_id,
                            variant_id: None,
                            value: Some("Blue".to_string())
                        }
                    },
                    FormProductVariantComplete {
                        variant: None,
                        product_variant: FormProductVariant {
                            id: Some(product_variant.id),
                            product_id: created_product_id,
                            variant_id: Some(product_variant.variant_id),
                            value: Some(50.to_string())
                        }

                    }
                ]
            },
            &connection
        ).unwrap();
        
        assert_eq!(
            serde_json::to_string(&show_product(created_product_id, &connection).unwrap()).unwrap(),
            serde_json::to_string(
                &(
                    Product {
                        id: 1,
                        name: "high heels".to_string(),
                        cost: 14.25,
                        active: false
                    },
                    vec![
                        (
                            ProductVariant {
                                id: 1,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "50".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 2,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "14".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 3,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "16".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 4,
                                variant_id: 1,
                                product_id: 1,
                                value: Some(
                                    "18".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        ),
                        (
                            ProductVariant {
                                id: 5,
                                variant_id: 2,
                                product_id: 1,
                                value: Some(
                                    "Blue".to_string(),
                                ),
                            },
                            Variant {
                                id: 2,
                                name: "color".to_string(),
                            }
                        )
                    ]
                )
            ).unwrap()
        );

        Ok(())
    });
}

fn delete_product(id: i32, conn: &SqliteConnection) ->  Result<i32> {
    use ::shoe_store::schema::products::dsl::products;
    diesel::delete(products.find(id))
        .execute(conn)?;

    Ok(id)
}

#[test]
#[should_panic(expected = "NotFound")]
fn delete_product_test() {
    use ::shoe_store::schema::products_variants::dsl::products_variants;
    let connection = establish_connection_test();
    connection.execute("PRAGMA foreign_keys = ON").unwrap();
    connection.test_transaction::<_, Error, _>(|| {
        let created_product_id =
            create_product(NewCompleteProduct {
                product: NewProduct {
                    name: "boots".to_string(),
                    cost: 13.23,
                    active: true
                },
                variants: vec![
                    NewVariantValue {
                        variant: NewVariant {
                            name: "size".to_string()
                        },
                        values: vec![
                            Some(12.to_string()),
                            Some(14.to_string()),
                            Some(16.to_string()),
                            Some(18.to_string())
                        ]
                    }
                ]
            }, 
            &connection).unwrap();
        
        delete_product(created_product_id, &connection).unwrap();

        let vec_product_variants = products_variants.load::<ProductVariant>(&connection)?;
        assert_eq!(vec_product_variants, vec![]);
        show_product(created_product_id, &connection).unwrap();

        Ok(())
    });
}

fn search_products(search: String, conn: &SqliteConnection) -> Result<Vec<(Product, Vec<(ProductVariant, Variant)>)>, Error> {
    use ::shoe_store::schema::products::dsl::*;
    use ::shoe_store::schema::variants::dsl::variants;

    let pattern = format!("%{}%", search);
    let products_result = 
        products
        .filter(name.like(pattern))
        .load::<Product>(conn)?;
    let variants_result =
        ProductVariant::belonging_to(&products_result)
            .inner_join(variants)
            .load::<(ProductVariant, Variant)>(conn)?
            .grouped_by(&products_result);
    let data = products_result.into_iter().zip(variants_result).collect::<Vec<_>>();

    Ok(data)
}

use diesel::TextExpressionMethods;

#[test]
fn search_products_test() {
    let connection = establish_connection_test();
    connection.test_transaction::<_, Error, _>(|| {
        let variants = vec![
            NewVariantValue {
                variant: NewVariant {
                    name: "size".to_string()
                },
                values: vec![
                    Some(12.to_string()),
                ]
            }
        ];

        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "boots".to_string(),
                cost: 13.23,
                active: true
            },
            variants: variants.clone()
        }, &connection).unwrap();
        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "high heels".to_string(),
                cost: 20.99,
                active: true
            },
            variants: variants.clone()
        }, &connection).unwrap();
        create_product(NewCompleteProduct {
            product: NewProduct {
                name: "running shoes".to_string(),
                cost: 10.99,
                active: true
            },
            variants: variants.clone()
        }, &connection).unwrap();

        assert_eq!(
            serde_json::to_string(&search_products("shoes".to_string(), &connection).unwrap()).unwrap(),
            serde_json::to_string(&vec![
                (
                    Product {
                        id: 3,
                        name: "running shoes".to_string(),
                        cost: 10.99,
                        active: true
                    },
                    vec![
                        (
                            ProductVariant {
                                id: 3,
                                variant_id: 1,
                                product_id: 3,
                                value: Some(
                                    "12".to_string(),
                                ),
                            },
                            Variant {
                                id: 1,
                                name: "size".to_string(),
                            }
                        )
                    ]
                )
            ]).unwrap()
        );

        Ok(())
    });
}
