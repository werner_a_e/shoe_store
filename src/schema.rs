table! {
    prices (id) {
        id -> Integer,
        name -> Text,
    }
}

table! {
    product_sales (id) {
        id -> Integer,
        sale_id -> Integer,
        product_id -> Integer,
        product_count -> Double,
        price -> Integer,
        tax -> Integer,
        discount -> Integer,
        sub_total -> Integer,
    }
}

table! {
    products (id) {
        id -> Integer,
        name -> Text,
        cost -> Double,
        active -> Bool,
    }
}

table! {
    products_prices (id) {
        id -> Integer,
        product_id -> Integer,
        price_id -> Integer,
        value -> Integer,
    }
}

table! {
    products_variants (id) {
        id -> Integer,
        variant_id -> Integer,
        product_id -> Integer,
        value -> Nullable<Text>,
    }
}

table! {
    sales (id) {
        id -> Integer,
        date -> Text,
        tax_total -> Integer,
        sub_total -> Integer,
        total -> Integer,
    }
}

table! {
    variants (id) {
        id -> Integer,
        name -> Text,
    }
}

joinable!(product_sales -> products (product_id));
joinable!(product_sales -> sales (sale_id));
joinable!(products_prices -> prices (price_id));
joinable!(products_prices -> products (product_id));
joinable!(products_variants -> products (product_id));
joinable!(products_variants -> variants (variant_id));

allow_tables_to_appear_in_same_query!(
    prices,
    product_sales,
    products,
    products_prices,
    products_variants,
    sales,
    variants,
);
